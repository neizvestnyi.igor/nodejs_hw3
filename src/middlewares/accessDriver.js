const jwt = require('jsonwebtoken');
require('dotenv').config();

function accessDriver(req, res, next) {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  if (decoded.role === 'DRIVER') {
    return next();
  }
  return res.status(401).json({ message: 'This service only for drivers!' });
}

module.exports = {
  accessDriver,
};
