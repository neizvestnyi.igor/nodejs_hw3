const { loadJoiValid } = require('../models/loads');

async function loadValidation(req, res, next) {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  try {
    await loadJoiValid.validateAsync({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    });

    return next();
  } catch (err) {
    return res.status(400).send({ message: `${err}` });
  }
}

module.exports = {
  loadValidation,
};
