const jwt = require('jsonwebtoken');
require('dotenv').config();
/**
 *
 * @param {*} req object
 * @param {*} res object
 * @param {*} next funv
 * @return {*} return object
 */
function accessShipper(req, res, next) {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  if (decoded.role === 'SHIPPER') {
    return next();
  }
  return res.status(401).json({ message: 'This service only for shippers!' });
}

module.exports = {
  accessShipper,
};
