const bcrypt = require('bcryptjs'); // Password hashing function
const jwt = require('jsonwebtoken');
// Достаємо нашу мангус модель:
const { Credentials } = require('../models/registrationCredentials');
const { User } = require('../models/users');

const getUser = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  User.findById(decoded._id, '-__v').orFail()
    .then((user) => {
      res.json({ user });
    }).catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const deleteUser = async (req, res) => {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  User.findByIdAndDelete(decoded._id).orFail()
    .then(async () => { await Credentials.findByIdAndDelete(decoded._id).orFail(); })
    .then(() => { res.status(200).send({ message: 'Profile deleted successfully' }); })
    .catch((err) => { res.status(400).send({ message: `${err}` }); });
};

const changePass = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);
  const userCred = await Credentials.findById(decoded._id);

  if (userCred && await bcrypt
    .compare(String(oldPassword), String(userCred.password))) {
    Credentials
      .findByIdAndUpdate(
        decoded._id,
        { $set: { password: await bcrypt.hash(newPassword, 10) } },
      ).orFail()
      .then(() => {
        res.status(200).send({ message: 'Password changed successfully' });
      }).catch((err) => {
        res.status(400).send({ message: `${err}` });
      });
  } else {
    res.status(400).send({ message: 'The old password is not correct' });
  }
};

module.exports = {
  getUser,
  deleteUser,
  changePass,
};
