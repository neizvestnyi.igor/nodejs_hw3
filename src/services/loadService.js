/* eslint-disable camelcase */
const jwt = require('jsonwebtoken');
const { Loads } = require('../models/loads');
const { Trucks } = require('../models/truck');

const addLoad = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  const load = new Loads({
    created_by: decoded._id,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  load.save()
    .then(() => res.json({ message: 'Load created successfully' }))
    .catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const getLoads = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  await Loads.find({ created_by: decoded._id }, '-__v')
    .then((loadsResult) => {
      res.json({ loads: loadsResult });
    })
    .catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const getLoadsById = async (req, res) => {
  await Loads.find({ created_by: req.params.id }, '-__v').orFail()
    .then((loadsResult) => {
      res.json({ loads: loadsResult });
    })
    .catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const putLoadsById = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  await Loads.findOneAndUpdate({
    _id: req.params.id,
    status: 'NEW',
    created_by: decoded._id,
  }, { $set: req.body }).orFail()
    .then(() => {
      res.json({ message: 'Load details changed successfully' });
    }).catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const deleteLoadsById = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  await Loads.findOneAndDelete({
    _id: req.params.id,
    status: 'NEW',
    created_by: decoded._id,
  }).orFail()
    .then(() => {
      res.json({ message: 'Load deleted successfully' });
    }).catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const getLoadById = async (req, res) => {
  await Loads.findOneAndUpdate({ _id: req.params.id }, '-__v').orFail()
    .then((loads) => {
      res.json({ load: loads });
    }).catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const postLoadById = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);
  // let truck;
  let loadWeight;
  let loadSize;
  let foundTRuck;

  try {
    await Loads.findOneAndUpdate({
      _id: req.params.id,
      status: 'NEW',
      created_by: decoded._id,
    }, {
      $set: { status: 'POSTED' },
      $push: {
        logs: {
          message: 'Load status changed to "POSTED"',
          time: new Date().toISOString(),
        },
      },
    })
      .then((resL) => {
        if (resL) {
          loadWeight = resL.payload;
          loadSize = resL.dimensions.width * resL.dimensions.length * resL.dimensions.height;
        }
      });

    if (loadWeight && loadSize) {
      await Trucks.findOneAndUpdate({
        status: 'IS',
        assigned_to: { $ne: 'Not assigned' },
      }, {
        $set: { status: 'OL' },
      })
        .where('payload')
        .gt(loadWeight)
        .where('size')
        .gt(loadSize)
        .then((result) => {
          console.log(result);
          foundTRuck = result;
        });
    } else {
      res.status(400).send({ message: 'The load was not found' });
    }

    if (foundTRuck) {
      await Loads.findOneAndUpdate({
        _id: req.params.id,
        status: 'POSTED',
        created_by: decoded._id,
      }, {
        $set: { status: 'ASSIGNED', state: 'En route to Pick Up', assigned_to: foundTRuck.assigned_to },
        $push: {
          logs: {
            message: 'Load status changed to "ASSIGNED"',
            time: new Date().toISOString(),
          },
        },
      }).orFail();

      res.json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    } else {
      await Loads.findOneAndUpdate({
        _id: req.params.id,
        status: 'POSTED',
        created_by: decoded._id,
      }, {
        $set: { status: 'NEW' },
        $push: {
          logs: {
            message: 'The truck was not found, load status changed to "NEW"',
            time: new Date().toISOString(),
          },
        },
      }).orFail();
      res.status(400).send({ message: 'The truck was not found' });
    }
  } catch (err) {
    res.status(400).send({ message: `${err}` });
  }
};

module.exports = {
  addLoad,
  getLoads,
  getLoadsById,
  putLoadsById,
  deleteLoadsById,
  getLoadById,
  postLoadById,
};
