/* eslint-disable camelcase */
const jwt = require('jsonwebtoken');
const { Trucks, truckJoiValid } = require('../models/truck');

const trucksPayload = {
  sprinter: {
    payload: 1700,
    size: 12750000,
  },
  smallStraight: {
    payload: 2500,
    size: 21250000,
  },
  largeStraight: {
    payload: 4000,
    size: 49000000,
  },
};

const addTruck = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);
  let truckParams;

  const { type } = req.body;
  if (type === 'SPRINTER') { truckParams = trucksPayload.sprinter; }
  if (type === 'SMALL STRAIGHT') { truckParams = trucksPayload.smallStraight; }
  if (type === 'LARGE STRAIGHT') { truckParams = trucksPayload.largeStraight; }

  try {
  // Якщо валідація не пройде, то буде помилка, яку перехопить catch:
    await truckJoiValid.validateAsync({ type });

    const truck = new Trucks({
      created_by: decoded._id,
      type,
      ...truckParams,
    });

    truck.save()
      .then(() => res.json({ message: 'Truck created successfully' }))
      .catch((err) => {
        res.status(400).send({ message: `${err}` });
      });
  } catch (err) {
    res.status(400).send({ message: `${err}` });
  }
};

const getTrucks = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  await Trucks.find({ created_by: decoded._id }, '-__v')
    .then((result) => {
      res.json({ trucks: result });
    })
    .catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const getTruckById = async (req, res) => {
  await Trucks.find({
    created_by: req.params.id,
    assigned_to: req.params.id,
  }, '-__v').orFail()
    .then((result) => {
      res.json({ truck: result });
    })
    .catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const truckUpdate = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);
  const { type } = req.body;
  let truckParams;

  if (type === 'SPRINTER') { truckParams = trucksPayload.sprinter; }
  if (type === 'SMALL STRAIGHT') { truckParams = trucksPayload.smallStraight; }
  if (type === 'LARGE STRAIGHT') { truckParams = trucksPayload.largeStraight; }

  try {
  // Якщо валідація не пройде, то буде помилка, яку перехопить catch:
    await truckJoiValid.validateAsync({ type });

    await Trucks.findOneAndUpdate({
      _id: req.params.id,
      status: 'IS',
      assigned_to: 'Not assigned',
      created_by: decoded._id,
    }, {
      $set: {
        type,
        ...truckParams,
      },
    }).orFail()
      .then(() => {
        res.json({ message: 'Truck details changed successfully' });
      }).catch((err) => {
        res.status(400).send({ message: `${err}` });
      });
  } catch (err) {
    res.status(400).send({ message: `${err}` });
  }
};

const deleteTruck = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  await Trucks.findOneAndDelete({
    _id: req.params.id,
    status: 'IS',
    assigned_to: 'Not assigned',
    created_by: decoded._id,
  }).orFail()
    .then(() => {
      res.json({ message: 'Truck deleted successfully' });
    }).catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

const assignTruck = async (req, res) => {
  // Field from headers:
  const { authorization } = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);

  await Trucks.findOneAndUpdate({
    assigned_to: decoded._id,
    status: 'IS',
  }, { $set: { assigned_to: 'Not assigned' } });

  await Trucks.findOneAndUpdate({
    _id: req.params.id,
    status: 'IS',
    assigned_to: 'Not assigned',
  }, { $set: { assigned_to: decoded._id } }).orFail()
    .then(() => {
      res.json({ message: 'Truck assigned successfully' });
    }).catch((err) => {
      res.status(400).send({ message: `${err}` });
    });
};

module.exports = {
  addTruck,
  getTrucks,
  getTruckById,
  truckUpdate,
  deleteTruck,
  assignTruck,
};
