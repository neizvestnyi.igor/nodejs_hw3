const bcrypt = require('bcryptjs'); // модуль для шифровання паролю
const jwt = require('jsonwebtoken');
require('dotenv').config();

// Достаємо нашу мангус модель:
const { Credentials, userJoiCredentials } = require('../models/registrationCredentials');
const { User } = require('../models/users');

const registerUser = async (req, res) => {
  const { email, password, role } = req.body;

  try {
    // Якщо валідація не пройде, то буде помилка, яку перехопить catch:
    await userJoiCredentials.validateAsync({ email, password, role });

    const userCred = new Credentials({
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });

    userCred.save().then((saved) => {
      const user = new User({
        _id: saved._id,
        role: saved.role,
        email: saved.email,
      });
      user.save();
    }).then(() => res.json({
      message: 'Profile created successfully',
    }));
  } catch (err) {
    res.status(400).send({ message: `${err}` });
  }
};

// Login, when we have user:
const loginUser = async (req, res) => {
  const user = await Credentials.findOne({ email: req.body.email });
  // If we have a user:
  // .compare - compares passwords:
  if
  (user && await bcrypt
    .compare(String(req.body.password), String(user.password))) {
    // Create {payload} for jwt:
    const payload = {
      email: user.email,
      _id: user._id,
      role: user.role,
    };
    // Create token:
    const jwtToken = jwt.sign(payload, process.env.secretkey);
    return res.json({ jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

const forgotPass = async (req, res) => {
  const user = await Credentials.findOne({ email: req.body.email });

  if (user) {
    return res.json({
      message: 'New password sent to your email address',
    });
  }

  return res.status(400).json({ message: 'Email not found' });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPass,
};
