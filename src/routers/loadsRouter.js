const express = require('express');

const router = express.Router();
const {
  addLoad,
  getLoads,
  getLoadsById,
  putLoadsById,
  deleteLoadsById,
  getLoadById,
  postLoadById,
} = require('../services/loadService');

const { authMiddleware } = require('../middlewares/authMiddleware');
const { accessShipper } = require('../middlewares/accessShipper');
const { loadValidation } = require('../middlewares/loadValidation');

router.post('/', authMiddleware, accessShipper, loadValidation, addLoad);
router.get('/', authMiddleware, accessShipper, getLoads);
router.get('/:id', authMiddleware, getLoadsById);
router.put('/:id', authMiddleware, accessShipper, loadValidation, putLoadsById);
router.delete('/:id', authMiddleware, accessShipper, deleteLoadsById);
router.get('/:id/shipping_info', authMiddleware, accessShipper, getLoadById);
router.post('/:id/post', authMiddleware, accessShipper, postLoadById);

module.exports = {
  loadsRouter: router,
};
