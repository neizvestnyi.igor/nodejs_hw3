const express = require('express');

const router = express.Router();
const { getUser, deleteUser, changePass } = require('../services/userService');
const { authMiddleware } = require('../middlewares/authMiddleware');

router.get('/me', authMiddleware, getUser);
router.delete('/me', authMiddleware, deleteUser);
router.patch('/me/password', authMiddleware, changePass);

module.exports = {
  usersRouter: router,
};
