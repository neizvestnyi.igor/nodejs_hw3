const express = require('express');

const router = express.Router();
const { registerUser, loginUser, forgotPass } = require('../services/authService');

router.post('/register', registerUser);
router.post('/login', loginUser);
router.post('/forgot_password', forgotPass);

module.exports = {
  authRouter: router,
};
