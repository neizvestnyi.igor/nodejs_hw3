const express = require('express');

const router = express.Router();
const {
  addTruck,
  getTrucks,
  getTruckById,
  truckUpdate,
  deleteTruck,
  assignTruck,
} = require('../services/trucksService');

const { authMiddleware } = require('../middlewares/authMiddleware');
const { accessDriver } = require('../middlewares/accessDriver');

router.post('/', authMiddleware, accessDriver, addTruck);
router.get('/', authMiddleware, accessDriver, getTrucks);
router.get('/:id', authMiddleware, accessDriver, getTruckById);
router.put('/:id', authMiddleware, accessDriver, truckUpdate);
router.delete('/:id', authMiddleware, accessDriver, deleteTruck);
router.post('/:id/assign', authMiddleware, accessDriver, assignTruck);

module.exports = {
  trucksRouter: router,
};
