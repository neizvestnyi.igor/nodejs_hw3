const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiCredentials = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),

  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/)
    .required(),

  role: Joi.string()
    .alphanum()
    .min(4)
    .max(20)
    .required(),
});

const userSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
});
// In which collection (Credentials) this model will be stored:
const Credentials = mongoose.model('Credentials', userSchema);

module.exports = {
  Credentials,
  userJoiCredentials,
};
