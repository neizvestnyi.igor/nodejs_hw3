const mongoose = require('mongoose');
const Joi = require('joi');

const loadJoiValid = Joi.object({
  created_by: Joi.string(),
  assigned_to: Joi.string(),
  status: Joi.string(),
  state: Joi.string(),
  name: Joi.string()
    .min(2).max(40)
    .required(),
  payload: Joi.number()
    .min(1).max(10000)
    .required(),
  pickup_address: Joi.string()
    .required(),
  delivery_address: Joi.string()
    .required(),
  dimensions: {
    width: Joi.number()
      .min(1).max(1000)
      .required(),
    length: Joi.number()
      .min(1).max(1000)
      .required(),
    height: Joi.number()
      .min(1).max(1000)
      .required(),
  },
});

const loadSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: Array,
    default: [],
  },
  created_date: {
    type: Date,
    default: Date.now,
  },

});
// In which collection (loads) this model will be stored:
const Loads = mongoose.model('loads', loadSchema);

module.exports = {
  Loads,
  loadJoiValid,
};
