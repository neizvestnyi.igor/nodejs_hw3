const mongoose = require('mongoose');
const Joi = require('joi');

const truckJoiValid = Joi.object({
  type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT').required(),
});

const truckSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: 'Not assigned',
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
  payload: {
    type: Number,
  },
  size: {
    type: Number,
  },

});
// In which collection (trucks) this model will be stored:
const Trucks = mongoose.model('trucks', truckSchema);

module.exports = {
  Trucks,
  truckJoiValid,
};
