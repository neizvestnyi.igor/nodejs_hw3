const express = require('express'); // Сервер
const morgan = require('morgan'); // Білбліотека для логування
require('dotenv').config();

const app = express(); // Створюємо ап нашого сервера
const mongoose = require('mongoose'); // ОРМ для монго БД

mongoose.connect(process.env.DB_PATH); // Шлях поклали в .env

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { loadsRouter } = require('./routers/loadsRouter');
const { trucksRouter } = require('./routers/trucksRouter');

// json, які надходять у body запиту, будуть записуватись у змінну req.body:
app.use(express.json());
app.use(morgan('tiny')); // Відобразить лог усіх дій у консолі

// По різним адресам додаємо різні роутери - продовження адреси та різні запити:
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/trucks', trucksRouter);

const start = async () => {
  try {
    app.listen(8080);
    console.log('Server running on port: 8080');
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
/**
 *
 * @param {*} err object
 * @param {*} res object
 */
function errorHandler(err, res) {
  res.status(500).send({ message: 'Server error' });
}

// ERROR HANDLER
app.use(errorHandler);
